import jwt
from flask import current_app
from datetime import timedelta,datetime
############加密的方法

def pyjwt_encode_data(data,expiry,secret=None):

    #给payload 添加过期时间
    # pyjwt 不像我们之前学习的那样 自动添加过期时间
    # 我们要自己添加

    # 过期时间
    # exp_time = datetime.now() + timedelta(seconds=current_app.config.get('JWT_EXPIRE'))
    # 重写定义一个字典
    _update_data = {'exp':expiry}

    # data['exp']=exp_time

    # 把data的数据 更新到 _update_data红
    _update_data.update(data)

    #秘钥
    if secret is None:
        secret = current_app.config.get('SECRET_KEY')

    # 生成token
    token = jwt.encode(_update_data,secret,algorithm='HS256')

    return token
###########解密的方法

def pyjwt_check_token(token,secret=None):
    # 秘钥
    if secret is None:
        secret = current_app.config.get('SECRET_KEY')
    try:

        data = jwt.decode(token,secret,algorithms=['HS256'])
    except Exception:
        return None
    else:

        return data
