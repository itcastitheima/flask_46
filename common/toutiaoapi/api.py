from flask_restful import Api
from collections import OrderedDict
from common.toutiaoapi.json import output_json

class BaseApi(Api):

    def __init__(self, app=None, prefix='',
                 default_mediatype='application/json', decorators=None,
                 catch_all_404s=False, serve_challenge_on_401=False,
                 url_part_order='bae', errors=None):

        #调用一下父类
        # 把当前类的 参数 传递给父类
        super().__init__(app=app,prefix=prefix,
                         default_mediatype=default_mediatype, decorators=decorators,
                         catch_all_404s=catch_all_404s, serve_challenge_on_401=serve_challenge_on_401,
                         url_part_order=url_part_order, errors=errors
                         )

        self.representations = OrderedDict([('application/json', output_json)])

