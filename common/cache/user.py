import json

from common.cache.constants import UserProfileCache
from toutiao import redis_cli
from flask import current_app
from common.models.user import User

#缓存用户的信息
class UserCache:

    def __init__(self,user_id):
        # 设置缓存的key
        self.key = 'user:{}:profile'.format(user_id)
        #属性
        self.user_id=user_id

    def save(self,user=None):

        if user is None:
            try:
                user = User.query.get(self.user_id)
            except Exception as e:
                current_app.logger.error(e)
                return None

        if user:
            #说明 传递的user有值
            # 对象转字典
            user_data = {
                'id': user.id,
                'mobile': user.mobile,
                'name': user.name,
                'photo': user.profile_photo or '',
                'is_media': user.is_media,
                'intro': user.introduction or '',
                'certi': user.certificate or '',
            }
            # 字典转字符串
            user_str = json.dumps(user_data)
            # redis保存
            # redis_cli.setex(key,seconds,value)
            current_app.redis_cli.setex(self.key,UserProfileCache.get_value(),user_str)

            return user_data

        return None

    def get(self):
        # 先查询缓存
        data = current_app.redis_cli.get(self.key)
        if data:
            # 如果缓存中有数据,则直接读取缓存
            # str=bytes.decode()
            return json.loads(data.decode())
        # 如果缓存中没有,则应该查询数据 缓存查询的数据
        return self.save()


##############################实现 登录用户的关注列表
from common.models.user import Relation
from time import time
class UserFollowingCache:

    def __init__(self,user_id):
        self.user_id=user_id
        self.key = 'user:{}:following'.format(user_id)
    # status = 1 表示 添加关注
    # status = 0 表示 取消关注
    def follow(self,target,status=1):

        if status == 1:
            # 添加关注
            # zset
            # ZADD key score member

            # zadd element/score pair

            current_app.redis_cli.zadd(self.key,{target:time()})

        else:
            # 取消关注
            current_app.redis_cli.zrem(self.key,target)




    def get_follow_list(self):

        # 1. 先查询redis缓存
        follow_list = current_app.redis_cli.zrange(self.key,0,-1)
        # {b'',b''}
        # 2. 判断是否存在缓存,存在则直接返回数据
        if follow_list:
            data_list=[]
            for item in follow_list:
                data_list.append(int(item))
            return data_list

            #return [int(item) for item in follow_list]
        # 3. 如果不存在缓存,则查询数据库

        try:
            relations = Relation.query.filter(
                Relation.user_id == self.user_id,
                Relation.relation == Relation.RELATION.FOLLOW
            ).all()
        except Exception as e:
            current_app.logger.error(e)
            return None

        data_list = []
        redis_mapping = {}
        for item in relations:
            data_list.append(item.target_user_id)
            # 为缓存做准备
            redis_mapping[item.target_user_id]=item.ctime.timestamp()
        # 4. 把查询的结果缓存起来
        if redis_mapping:
            current_app.redis_cli.zadd(self.key,redis_mapping)


        return data_list








##############################实现 登录用户对文章的态度
from common.models.news import Attitude
class UserAttitudeCache:

    def __init__(self,user_id):

        self.user_id=user_id
        self.key = 'user:{}​:article:​attitude'.format(user_id)


    # 实现数据的修改(增删改)
    def update(self,article_id,attitude,status=1):

        if status == 1:
            # 增加
            current_app.redis_cli.hset(self.key,article_id,attitude)

        else:
            # 删除
            current_app.redis_cli.hdel(self.key,article_id)



    def get_list(self):
        # 1. 先查询redis缓存
        data = current_app.redis_cli.hgetall(self.key)
        # {b'':b'',b'':b''}
        # 2. 判断是否存在缓存,存在则直接返回数据
        if data:

            data_dict = {}

            for key,value in data.items():
                data_dict[int(key)]=int(value)

            return data_dict
            # return {int(key):int(value)  for key,value in data.items()}

        # 3. 如果不存在缓存,则查询数据库
        attitudes = Attitude.query.filter(
            Attitude.user_id == self.user_id,
            Attitude.attitude != None
        ).all()

        data_dict = {}
        for item in attitudes:
            data_dict[int(item.article_id)]=int(item.attitude)
        # 4. 把查询的结果缓存起来

        if data_dict:
            current_app.redis_cli.hmset(self.key,data_dict)


        return data_dict




######################登录用户对评论 是否点赞
from common.models.news import CommentLiking
from .constants import CommentLikingCacheTTL

class CommentLikingCache:

    def __init__(self,user_id):
        self.user_id = user_id
        self.key = 'user:{}:comm:liking'.format(user_id)


    # 方法1 当用户点赞 或取消点赞 更新数据
    # status = 1  用户点赞
    # status = 0 取消点赞
    def update(self,comment_id,status=1):

        if status == 1:
            current_app.redis_cli.sadd(self.key,comment_id)
        else:
            current_app.redis_cli.srem(self.key,comment_id)

    # 方法2 获取集合数据
    def get_list(self):
        # 1. 先查询redis缓存
        data = current_app.redis_cli.smembers(self.key)
        # {b'',b''}
        # 2. 判断是否存在缓存,存在则直接返回数据
        if data:
            return [ int(item) for item in data]

        # 3. 如果不存在缓存,则查询数据库
        commentlikings = CommentLiking.query.filter(
            CommentLiking.user_id == self.user_id,
            CommentLiking.is_deleted == False
        ).all()

        data=[]
        for item in commentlikings:
            data.append(item.comment_id)
        # 4. 把查询的结果缓存起来

        current_app.redis_cli.sadd(self.key,*data)
        current_app.redis_cli.expire(self.key,CommentLikingCacheTTL.get_value())

        return data


###############################用户收藏列表缓存
from common.models.news import Collection
from common.cache.constants import UserCollectionCacheTTL

class UserCollectionCache:

    def __init__(self,user_id):

        self.user_id = user_id
        self.key = 'user:{}:collection'.format(user_id)


    def get_page_collection(self,page,per_page):

        # zset
        # 1. 先查询redis缓存
        count = current_app.redis_cli.zcard(self.key)

        # -1 是我们特殊规定.如果传递了 -1 则表示获取所有收藏id
        if count > 0 and per_page == -1:
            data = current_app.redis_cli.zrevrange(self.key, 0, -1)
            return [ int(item) for item in data]
        #获取有序集合中的总数量
        # []
        if count > 0:
            data = current_app.redis_cli.zrevrange(self.key, (page - 1) * per_page, page * per_page - 1)
            return count,[ int(item) for item in data]
        # 2. 判断是否存在缓存,存在则直接返回数据
        # 3. 如果不存在缓存,则查询数据库
        # 3.1 查询收藏数据--为了后边缓存逻辑,少改代码--查询所有的收藏数据
        collections = Collection.query.filter(
            Collection.user_id == self.user_id,
            Collection.is_deleted == False
        ).all()

        # 3.2. 将对象列表转换为字典列表
        results = []
        mapping = {}
        for collection in collections:
            # 我们只获取 收藏数据对应的文章id
            # 获取了文章id之后,我们的文章之前缓存过
            # 所以 从redis中获取缓存
            results.append(collection.article_id)

            #为缓存准备
            mapping[collection.article_id]=collection.utime.timestamp()

        # 4. 把查询的结果缓存起来
        # element/score pair
        # mapping[element]=score

        current_app.redis_cli.zadd(self.key,mapping)
        current_app.redis_cli.expire(self.key,UserCollectionCacheTTL.get_value())
        if len(results) > 0 and per_page == -1:
            return results
        return len(results),results[(page-1)*per_page:page*per_page]


    def get_list(self):

        # data = current_app.redis_cli.zrange(self.key,0,-1)
        return self.get_page_collection(page=1,per_page=-1)