
#一起是这样写 这样写 如果需求变化了 不容易改代码
USER_BASIC_INFO_EXPIRE = 3600


class BaseCacheTTL:

    TTL = 0

    @classmethod
    def get_value(cls):

        return cls.TTL


class UserProfileCache(BaseCacheTTL):

    TTL = 7200


class AllChannelCacheTTL(BaseCacheTTL):

    TTL = 7200
class UserChannelCacheTTL(BaseCacheTTL):

    TTL = 7200

class DefaultChannelCacheTTL(BaseCacheTTL):

    TTL = 24*3600

class ArticleDetailCacheTTL(BaseCacheTTL):

    TTL = 24*3600


class CommentLikingCacheTTL(BaseCacheTTL):

    TTL = 3600



class UserCollectionCacheTTL(BaseCacheTTL):

    TTL = 3600