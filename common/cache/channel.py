from flask import current_app

from common.cache.constants import AllChannelCacheTTL, UserChannelCacheTTL, DefaultChannelCacheTTL
from common.models.news import Channel,UserChannel
import json

class AllChannelCache(object):

    key = 'all:ch'

    @classmethod
    def get_all_channel(cls):

        """
        # 1. 先查询redis缓存
        # 2. 判断是否存在缓存,存在则直接返回数据
        # 3. 如果不存在缓存,则查询数据库
        # 4. 把查询的结果缓存起来
        :return:
        """
        # 1. 先查询redis缓存
        data = current_app.redis_cli.get(cls.key)
        if data:
            # 2. 判断是否存在缓存,存在则直接返回数据
            return json.loads(data.decode())

        # 3. 如果不存在缓存,则查询数据库
        # 3.1. 获取查询结果集
        # channels = Channel.query.all()
        channels = Channel.query.filter(Channel.is_visible == True).order_by(Channel.sequence).all()

        # 3.2. 对象转字典
        channels_list = []
        for item in channels:
            channels_list.append({
                'id': item.id,
                'name': item.name
            })
        # 3.3 . 把查询的结果缓存起来
        data = {'channels': channels_list}

        current_app.redis_cli.setex(cls.key,AllChannelCacheTTL.get_value(),json.dumps(data))

        # 3.4. 返回响应
        return data



class UserChannelCache(object):


    def __init__(self,user_id):
        self.user_id=user_id
        self.key = 'user:{}:ch'.format(user_id)


    def get(self):
        # 1. 先查询redis缓存
        data = current_app.redis_cli.get(self.key)
        # 2. 判断是否存在缓存,存在则直接返回数据
        if data:
            return json.loads(data.decode())
        # 3. 如果不存在缓存,则查询数据库
        #  查询用户关注频道
        channels = UserChannel.query.filter(
            UserChannel.user_id == self.user_id,
            UserChannel.is_deleted == False
        ).order_by(UserChannel.sequence).all()
        # 将查询结果集转换为字典列表
        channels_list = []
        for item in channels:
            channels_list.append({
                'id': item.channel.id,
                'name': item.channel.name
            })

        channels_list.insert(0,{
            'id':0,
            'name':'推荐'
        })

        # 把查询的结果缓存起来
        data = {'channels': channels_list}
        current_app.redis_cli.setex(self.key,UserChannelCacheTTL.get_value(),json.dumps(data))
        #  返回响应
        return data


    def clear(self):

        current_app.redis_cli.delete(self.key)

class DefaultChannelCache(object):

    key = 'default:ch'

    @classmethod
    def get_default_channel(cls):
        # 1. 先查询redis缓存
        data = current_app.redis_cli.get(cls.key)

        # 2. 判断是否存在缓存,存在则直接返回数据
        if data:
            return json.loads(data.decode())
        # 3. 如果不存在缓存,则查询数据库
        channels = Channel.query.filter(
            Channel.is_default == True,
            Channel.is_visible == True
        ).order_by(Channel.sequence).all()

        channels_list = []
        for item in channels:
            channels_list.append({
                'id': item.id,
                'name': item.name
            })

        channels_list.insert(0, {
            'id': 0,
            'name': '推荐'
        })
        data = {'channels': channels_list}

        # 4. 把查询的结果缓存起来
        current_app.redis_cli.setex(cls.key,DefaultChannelCacheTTL.get_value(),json.dumps(data))


        return data