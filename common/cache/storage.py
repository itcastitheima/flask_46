# 永久存储 [不设置过期时间]

from flask import current_app

class CountStorageBase(object):

    key = ''


    @classmethod
    def update(cls,article_id,incre=1):
        # ZINCRBY key increment member
        #
        # 为有序集 key 的成员 member 的 score 值加上增量 increment 。
        current_app.redis_cli.zincrby(cls.key,incre,article_id)

    @classmethod
    def get_score(cls,article_id):

        return current_app.redis_cli.zscore(cls.key,article_id)


class ArticleReadingCountStorage(CountStorageBase):

    key = 'count:art:reading'

class ArticleCollectingCountStorage(CountStorageBase):
    """
    文章收藏数量
    """
    key = 'count:art:collecting'

class ArticleDislikeCountStorage(CountStorageBase):
    """
    文章不喜欢数据
    """
    key = 'count:art:dislike'

class ArticleLikingCountStorage(CountStorageBase):
    """
    文章点赞数据
    """
    key = 'count:art:liking'

class ArticleCommentCountStorage(CountStorageBase):
    """
    文章评论数量
    """
    key = 'count:art:comm'

class CommentReplyCountStorage(CountStorageBase):
    """
    评论回复数量
    """
    key = 'count:art:reply'