from flask import current_app
import json

from common.cache.constants import ArticleDetailCacheTTL
from common.models.news import Article

from flask_restful import fields,marshal

article_fields = {
    'art_id': fields.Integer(attribute='id'),
    'title': fields.String(attribute='title'),
    'pubdate': fields.DateTime(attribute='ctime', dt_format='iso8601'),
    'content': fields.String(attribute='content.content'),
    'aut_id': fields.Integer(attribute='user_id'),
    'ch_id': fields.Integer(attribute='channel_id'),
}

class ArticleCache:

    def __init__(self,article_id):
        self.article_id=article_id
        self.key='art:{}:detail'.format(article_id)



    def get_detail(self):
        # 1. 先查询redis缓存
        data=current_app.redis_cli.get(self.key)

        # 2. 判断是否存在缓存,存在则直接返回数据
        if data:
            return json.loads(data.decode())
        # 3. 如果不存在缓存,则查询数据库
        # 3.1. 根据文章id查询文章详情信息
        try:
            # article = Article.query.get(article_id)
            article = Article.query.filter(
                Article.status == Article.STATUS.APPROVED,
                Article.id == self.article_id
            ).first()
        except Exception as e:
            current_app.logger.error(e)
            article = None

        if article is None:
            return None

        # 把对象转换为字典
        # article_dict = {
        #     'art_id':article.id
        #
        # }
        article_dict = marshal(article, article_fields)

        # 补充作者信息
        article_dict['aut_name'] = article.user.name
        article_dict['aut_photo'] = article.user.profile_photo

        # 4. 把查询的结果缓存起来
        if article_dict:
            current_app.redis_cli.setex(self.key,ArticleDetailCacheTTL.get_value(),json.dumps(article_dict))


        return article_dict
