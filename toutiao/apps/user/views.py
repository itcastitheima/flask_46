from flask_restful import Resource

class UserResouce(Resource):

    def get(self):


        return {'msg':'user get'}


"""
需求:
        实现短信验证码的发送
前端:
        当用户输入完手机号之后,会点击获取验证码
        前端需要发送ajax请求.   GET      app/v1_0/sms/codes/<**mobile**>/
后端:
        接收数据
        验证数据
        生成短信验证码
        保存短信验证码
        实现短信验证码的发送[省略]
        返回响应
"""
from random import randint
from string import digits
from random import choice
from toutiao import redis_cli
class SmsCodeResouce(Resource):

    def get(self,mobile):
        # 传统的正则验证

        # 生成短信验证码
        # code = '%06d'%randint(0,999999)

        code = ''.join([choice(digits) for _ in range(6)])
        # 保存短信验证码  redis
        # redis_cli.setex(key,seconds,value)
        from .consts import  SMS_CODE_EXPIRES
        redis_cli.setex(mobile,SMS_CODE_EXPIRES,code)
        # 返回响应
        return {'mobile':mobile}


"""
需求:
        实现登录注册功能
前端:
        当用户输入完手机号和密码之后,点击登录按钮
        登录按钮会发送ajax请求.  
后端:
        
        POST
        
        1.接收数据
        2.验证数据
        3.根据手机号进行数据的查询
        4.如果查询有结果,则进行登录
        5.如果查询没有结果,则进行注册
        6.生成token,返回响应


"""
from flask import request
from flask_restful import reqparse
import re
from flask_restful import inputs
from common.models.user import User
from datetime import datetime
from toutiao import db
from flask import current_app

def check_mobile(value):

    if not re.match('1[3-9]\d',value):
        raise ValueError('数据不满足要求')

    return value
def check_code(value):

    # 1. 验证位数

    # 2. 和redis中的进行比对

    return value

class UserLoginResource(Resource):

    def post(self):

        # 1.接收数据
        # mobile = request.json.get('mobile')
        # sms_code = request.json.get('code')
        # 2.验证数据
        rp = reqparse.RequestParser()
        # rp.add_argument('mobile',required=True,location='json',type=check_mobile,help='数据不满足要求')
        rp.add_argument('mobile',required=True,location='json',type=inputs.regex('1[3-9]\d{9}'),help='数据不满足要求')
        rp.add_argument('code',required=True,location='json',type=inputs.regex('\d{6}'))

        validated_data = rp.parse_args()

        mobile=validated_data.get('mobile')

        # 3.根据手机号进行数据的查询
        user = None
        try:
            user=User.query.filter_by(mobile=mobile).first()
            # user=User.query.filter(User.mobile==mobile).first()
        except Exception as e:
            # current_app.logger
            # 获取当前 flask实例对象的 logger
            current_app.logger.error(e)


        if user:
            # 4.如果查询有结果,则进行登录
            # 添加一下 最后登录时间
            user.last_login=datetime.now()

            db.session.commit()
        else:
            # 5.如果查询没有结果,则进行注册
            #这里 生成雪花算法 实例对象
            # from common.snowflake.id_worker import IdWorker
            # worker = IdWorker(1, 2, 0)
            # from toutiao import worker

            user = User(
                # id=worker.get_id(),
                id=current_app.worker.get_id(),
                mobile=mobile,
                name=mobile
            )
            db.session.add(user)
            db.session.commit()
        # 6.生成token,返回响应
        from toutiao.utils.generic_token import generic_login_token
        token,refresh_token = generic_login_token(user.id)

        # 缓存 用户信息
        from common.cache.user import UserCache
        UserCache(user.id).save(user)

        return {'token':token,'refresh_token':refresh_token}


    def put(self):

        # 判断 g.user_id 和 g.is_refresh
        # 必须有用户信息 而且刷新token为True 我们就可以给它生成 2个小时的token
        if g.user_id and g.is_refresh:
            from toutiao.utils.generic_token import generic_login_token
            token,refresh_token = generic_login_token(g.user_id)

            return {'token':token}

        return {},403


from common.cache.user import UserCache
from toutiao.utils.decoretors import login_required
from flask import g
class UserCenterResource(Resource):

    method_decorators = [login_required]

    def get(self):


        # 1. 必须是登录用户
        # 假如 我已经是登录用户了 已经获取了 用户token了
        user_id = g.user_id
        # 2. 根据用户id 获取数据
        data = UserCache(user_id).get()
        # 3. 返回响应
        return data




















































#查询数据
class UserInfoResource(Resource):

    def get(self):
        user = User.query.filter_by(id=1).first()

        user_data = {
            'id': user.id,
            'mobile': user.mobile,
            'name': user.name,
            'photo': user.profile_photo or '',
            'is_media': user.is_media,
            'intro': user.introduction or '',
            'certi': user.certificate or '',
        }

        return user_data



import json
#查询redis缓存
class CacheUserInfoResource(Resource):

    def get(self):
        redis_data=redis_cli.get(1)
        user_data=json.loads(redis_data.decode())
        return user_data
    #用户第一次将数据库中的数据保存到缓存
    def post(self):
        user = User.query.filter_by(id=1).first()

        user_data = {
            'id': user.id,
            'mobile': user.mobile,
            'name': user.name,
            'photo': user.profile_photo or '',
            'is_media': user.is_media,
            'intro': user.introduction or '',
            'certi': user.certificate or '',
        }

        redis_cli.set(1, json.dumps(user_data))

        return {'message':'ok'}

"""

用户信息的缓存

string      --
hash        --
list
set
zset

"""