from flask.blueprints import Blueprint
# from flask_restful import Api
from common.toutiaoapi.api import BaseApi

# 1. 创建蓝图实例对象
user_bp=Blueprint(name='user',import_name=__name__)
# 2. 创建Api实例对象
user_api=BaseApi(user_bp)


from .views import UserResouce,SmsCodeResouce,UserLoginResource,UserCenterResource
# user_api.add_resource(UserResouce,'/user')
#发送短信验证码
user_api.add_resource(SmsCodeResouce,'/app/v1_0/sms/codes/<phone:mobile>/')

#注册登录
user_api.add_resource(UserLoginResource,'/app/v1_0/authorizations')

#获取登录用户的信息
user_api.add_resource(UserCenterResource,'/app/v1_0/user')


#############压力测试
#路由
# mysql 获取
user_api.add_resource(views.UserInfoResource,'/user')

# redis 获取
user_api.add_resource(views.CacheUserInfoResource,'/user/cache')

################################################
#设置 响应统一数据

# from flask_restful.representations import json
#
# from flask import make_response, current_app
# from flask_restful.utils import PY3
# from json import dumps
#
# @user_api.representation('application/json')
# def output_json(data, code, headers=None):
#     """Makes a Flask response with a JSON encoded body"""
#
#     settings = current_app.config.get('RESTFUL_JSON', {})
#
#     # If we're in debug mode, and the indent is not set, we set it to a
#     # reasonable value here.  Note that this won't override any existing value
#     # that was set.  We also set the "sort_keys" value.
#     if current_app.debug:
#         settings.setdefault('indent', 4)
#         settings.setdefault('sort_keys', not PY3)
#
#     data = {
#         'message':'ok',
#         'data':data
#     }
#
#     # always end the json dumps with a new line
#     # see https://github.com/mitsuhiko/flask/pull/1262
#     dumped = dumps(data, **settings) + "\n"
#
#     resp = make_response(dumped, code)
#     resp.headers.extend(headers or {})
#     return resp
