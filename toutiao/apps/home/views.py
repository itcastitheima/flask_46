from flask_restful import Resource

# 定义类视图
class HomeResouce(Resource):

    def get(self):

        return {'msg':'home get'}

from common.models.news import Channel
from common.cache.channel import AllChannelCache
class AllChannelResouce(Resource):

    def get(self):

        return AllChannelCache.get_all_channel()


from toutiao.utils.decoretors import login_required
from common.models.news import UserChannel
from flask import g
from common.cache.channel import UserChannelCache,DefaultChannelCache
from toutiao import db
from flask import request

class UserChannelResouce(Resource):

    # method_decorators = [login_required]
    method_decorators = {
        'put': [login_required]
    }

    def get(self):

        # 1. 获取用户id
        user_id = g.user_id

        if user_id:
            # 登录用户 返回 用户关注频道
            return UserChannelCache(user_id).get()
        else:
            # 未登录 用户 返回默认频道
            return DefaultChannelCache.get_default_channel()



    def put(self):

        # 1. 获取用户id
        user_id = g.user_id
        # 2. 把当前用户关注的频道 全部删除
        UserChannel.query.filter(
            UserChannel.user_id==user_id
        ).delete()
        db.session.commit()
        # 3. 把当前 前端传递过来的频道 全部添加
        channels = request.json.get('channels')
        for item in channels:
            # item = {id: 2, seq: 2}
            uc = UserChannel(
                user_id=user_id,
                channel_id=item.get('id'),
                sequence=item.get('seq')
            )
            db.session.add(uc)
        db.session.commit()

        #删除缓存.因为缓存已经 不对了
        UserChannelCache(user_id).clear()

        return channels


from common.models.news import Article

class ArticlesResouce(Resource):

    def get(self):

        """
        1. 获取频道id  channel_id 如果是0 我们把 chnanel_id 设置为1
        2. 根据频道id查询文章数据
        3. 将文章对象转换为字典
        4. 返回响应
        {
         "pre_timestamp": 0,
         "results": []
        }
        :return:
        """
        # 1. 获取频道id  channel_id 如果是0 我们把 chnanel_id 设置为1
        channel_id = request.args.get('channel_id',1)
        if int(channel_id) == 0:
            channel_id = 1
        # 2. 根据频道id查询文章数据
        articles=Article.query.filter(Article.channel_id == channel_id).all()
        # 3. 将文章对象转换为字典
        articles_list = []
        # from common.cache.storage import ArticleReadingCountStorage
        for item in articles:


            # count = ArticleReadingCountStorage.get_score(item.id)

            articles_list.append({
                "art_id": item.id,
                "title": item.title,
                "aut_id": item.user.id,
                "pubdate": item.ctime.strftime('%Y-%m-%d %H:%M:%S'),
                "aut_name": item.user.name,
                "comm_count": item.comment_count,
                "is_top": False,
                'cover': item.cover,
            })
        # 4. 返回响应
        return {
            'pre_timestamp':0,
            'results':articles_list
        }
        # {
        #  "pre_timestamp": 0,
        #  "results": []
        # }
        # pass
from common.models.news import Article
from flask import current_app,abort

# from flask_restful import fields,marshal
#
# article_fields = {
#     'art_id': fields.Integer(attribute='id'),
#     'title': fields.String(attribute='title'),
#     'pubdate': fields.DateTime(attribute='ctime', dt_format='iso8601'),
#     'content': fields.String(attribute='content.content'),
#     'aut_id': fields.Integer(attribute='user_id'),
#     'ch_id': fields.Integer(attribute='channel_id'),
# }

class DetailResouce(Resource):

    def get(self,article_id):
        """
        1. 根据文章id查询文章详情信息
        2. 根据用户是否登录,判断登录用户 是否 关注了该作者,是否 点赞该文章 是否 不喜欢该文章 是否 收藏该文章
        3. 返回数据
        :param article_id:
        :return:
        """
        # # 1. 根据文章id查询文章详情信息
        # try:
        #     # article = Article.query.get(article_id)
        #     article = Article.query.filter(
        #         Article.status == Article.STATUS.APPROVED,
        #         Article.id == article_id
        #     ).first()
        # except Exception as e:
        #     current_app.logger.error(e)
        #     article = None
        #
        # if article is None:
        #     abort(404)
        #
        # # 把对象转换为字典
        # # article_dict = {
        # #     'art_id':article.id
        # #
        # # }
        # article_dict = marshal(article,article_fields)
        #
        # # 补充作者信息
        # article_dict['aut_name']=article.user.name
        # article_dict['aut_photo']=article.user.profile_photo

        from common.cache.article import ArticleCache
        article_dict = ArticleCache(article_id).get_detail()

        if article_dict is None:
            abort(404)

        # 2. 根据用户是否登录,判断登录用户 是否 关注了该作者,是否 点赞该文章 是否 不喜欢该文章 是否 收藏该文章
        # 后边会分别实现这些功能
        is_followed= False
        attitude= -1 # 不喜欢0 喜欢1 无态度-1
        is_collected=False

        if g.user_id:
            from common.cache.user import UserFollowingCache
            # 我们判断 登录用户
            followed_list = UserFollowingCache(g.user_id).get_follow_list()
            is_followed = article_dict['aut_id'] in followed_list

            # 获取态度列表
            from common.cache.user import UserAttitudeCache
            attitude_dict = UserAttitudeCache(g.user_id).get_list()

            attitude =  attitude_dict.get(int(article_id))


            from common.cache.user import UserCollectionCache
            data_list = UserCollectionCache(g.user_id).get_list()
            is_collected = int(article_id) in  data_list

        article_dict['is_followed']=is_followed
        article_dict['attitude']=attitude
        article_dict['is_collected']=is_collected


        # 让统计数据的阅读量 +1
        from common.cache.storage import ArticleReadingCountStorage

        ArticleReadingCountStorage.update(article_id)

        # 3. 返回数据
        return article_dict
"""
需求
    :实现关注作者. 不能自己关注自己

前端
    当登录用户 点击关注的时候,前端发送ajax 请求 
    POST  请求数据中需要携带 关注的用户id (target)
    JSON

后端
    1. 必须是登录用户才可以操作
    2. 接收数据
    3. 验证数据
    4. 数据入库
    5. 返回响应
"""
from common.models.user import User,Relation
from flask_restful import reqparse
class FollowingsResouce(Resource):

    method_decorators = [login_required]

    def post(self):

        # target=request.json.get('target')
        # try:
        #     user = User.query.get(target)
        # except Exception as e:
        #     current_app.logger.error(e)
        #     user = None
        # if user is None:
        #     abort(404)



        # 1. 创建 验证实例对象
        rp = reqparse.RequestParser()
        # 2. 添加验证字段
        rp.add_argument('target',location='json',required=True,type=int,help='没有传递关注用户id')
        # 3. 验证
        data = rp.parse_args()
        # 4. 获取验证之后的数据
        target_user_id = data.get('target')

        # 不允许 自己关注自己.因为我们的数据比较少,就是测试一下
        # if g.user_id == target_user_id:
        #     abort(400)

        relation = Relation.query.filter(
            Relation.user_id == g.user_id,
            Relation.target_user_id == target_user_id
        ).first()

        if relation is None:

            # 新增数据
            relation=Relation(
                user_id=g.user_id,
                target_user_id=target_user_id,
                relation=Relation.RELATION.FOLLOW
            )

            db.session.add(relation)
            db.session.commit()
        else:
            relation.relation = Relation.RELATION.FOLLOW
            db.session.commit()

        # 添加关注
        from common.cache.user import UserFollowingCache
        UserFollowingCache(g.user_id).follow(target_user_id)

        return {'target':target_user_id}


class UnFollowingsResouce(Resource):

    method_decorators = [login_required]

    def delete(self,target):

        """
        1. 接收数据
        2. 验证数据
        3. 查询数据
        4. 删除数据
        5. 返回响应
        :param target:
        :return:
        """
        # 1. 接收数据
        # 2. 验证数据
        # 3. 查询数据
        relation=Relation.query.filter(
            Relation.user_id == g.user_id,
            Relation.target_user_id == target
        ).first()
        # 4. 删除数据
        # 物理删除
        # db.session.delete(relation)
        # db.session.commit()
        if relation:
            relation.relation=Relation.RELATION.DELETE
            db.session.commit()

            from common.cache.user import UserFollowingCache
            # 把缓存中的 关注数据 删除
            UserFollowingCache(g.user_id).follow(target,status=0)

        # 5. 返回响应
        return {'target':target}

# 点赞
from common.models.news import Attitude
class LikingResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        """
        1. 接收数据
        2. 验证数据
        3. 查询数据
        4. 如果数据存在,则更新状态
        5. 如果数据不存在,则新增数据
        6. 返回响应
        :return:
        """
        # 1. 接收数据
        # 2. 验证数据
        target=request.json.get('target')

        # 3. 查询数据
        at = Attitude.query.filter(
            Attitude.user_id == g.user_id,
            Attitude.article_id == target
        ).first()
        if at:
            # 4. 如果数据存在,则更新状态
            at.attitude = Attitude.ATTITUDE.LIKING
            db.session.commit()
        else:
            # 5. 如果数据不存在,则新增数据
            at=Attitude(
                user_id=g.user_id,
                article_id=target,
                attitude=Attitude.ATTITUDE.LIKING
            )

            db.session.add(at)
            db.session.commit()

        from common.cache.user import UserAttitudeCache
        UserAttitudeCache(g.user_id).update(target,1)
        # 6. 返回响应
        return {'target':target}



class DisLikingResouce(Resource):

    method_decorators = [login_required]

    def delete(self,target):

        at = Attitude.query.filter(
            Attitude.user_id == g.user_id,
            Attitude.article_id == target
        ).first()
        if at:
            # 4. 如果数据存在,则更新状态
            at.attitude = None
            db.session.commit()

        from common.cache.user import UserAttitudeCache
        UserAttitudeCache(g.user_id).update(target, 1,status=0)
        return {'target':target}



class DislikesResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        """
        1. 接收数据
        2. 验证数据
        3. 查询数据
        4. 如果数据存在,则更新状态
        5. 如果数据不存在,则新增数据
        6. 返回响应
        :return:
        """
        # 1. 接收数据
        # 2. 验证数据
        target=request.json.get('target')

        # 3. 查询数据
        at = Attitude.query.filter(
            Attitude.user_id == g.user_id,
            Attitude.article_id == target
        ).first()
        if at:
            # 4. 如果数据存在,则更新状态
            at.attitude = Attitude.ATTITUDE.DISLIKE
            db.session.commit()
        else:
            # 5. 如果数据不存在,则新增数据
            at=Attitude(
                user_id=g.user_id,
                article_id=target,
                attitude=Attitude.ATTITUDE.LIKING
            )

            db.session.add(at)
            db.session.commit()

        from common.cache.user import UserAttitudeCache
        UserAttitudeCache(g.user_id).update(target, 0)
        # 6. 返回响应
        return {'target':target}



class UnDislikesResouce(Resource):

    method_decorators = [login_required]

    def delete(self,target):

        at = Attitude.query.filter(
            Attitude.user_id == g.user_id,
            Attitude.article_id == target
        ).first()
        if at:
            # 4. 如果数据存在,则更新状态
            at.attitude = None
            db.session.commit()

        from common.cache.user import UserAttitudeCache
        UserAttitudeCache(g.user_id).update(target, 1,status=0)

        return {'target':target}

"""
需求:
        实现登录用户的收藏功能
前端:
        当登录用户点击收藏之后,应该给后端发送ajax请求
后端:
        POST        article_id  json   body
        
        1. 接收数据
        2. 验证数据
        3. 根据条件查询数据.查看数据是否存在
        4. 如果数据不存在,则新增
        5. 如果数据存在,则更新
        6. 返回响应

"""
from common.models.news import Collection
class CollectionResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        # 1. 接收数据
        # 2. 验证数据
        #文章id
        target=request.json.get('target')
        # 3. 根据条件查询数据.查看数据是否存在
        try:
            ct = Collection.query.filter(
                Collection.user_id == g.user_id,
                Collection.article_id == target
            ).first()
        except Exception as e:
            current_app.logger.error(e)
            ct = None

        if ct is None:
            # 4. 如果数据不存在,则新增
            ct = Collection(
                user_id=g.user_id,
                article_id=target
            )
            db.session.add(ct)
            db.session.commit()
        else:
            # 5. 如果数据存在,则更新
            ct.is_deleted=False
            db.session.commit()
        # 6. 返回响应
        return {'target':target}


    def get(self):
        """
        1. 必须是登录用户
        2. 获取查询字符串的分页数据
        3. 查询收藏数据--查询所有的收藏数据
        4. 将对象列表转换为字典列表
        5. 分页数据
        6. 返回响应

        :return:
        """
        # 1. 必须是登录用户
        # 2. 获取查询字符串的分页数据
        page = request.args.get('page',1)       # 第几页的数据
        per_page = request.args.get('per_page',2) #每一页多少条数据

        try:
            page =int(page)
            per_page = int(per_page)
        except Exception:
            page = 1
            per_page = 2

        # # 3. 查询收藏数据--为了后边缓存逻辑,少改代码--查询所有的收藏数据
        # collections = Collection.query.filter(
        #     Collection.user_id == g.user_id,
        #     Collection.is_deleted == False
        # ).all()
        #
        # # 4. 将对象列表转换为字典列表
        # results = []
        # for collection in collections:
        #     # 我们只获取 收藏数据对应的文章id
        #     # 获取了文章id之后,我们的文章之前缓存过
        #     # 所以 从redis中获取缓存
        #     results.append(collection.article_id)

        # 5. 分页数据
        # results = [1,2,3,4,5,6,7,8]
        # page = 1          2
        # per_page = 2      2
        # page_results = results[(page-1)*per_page: page*per_page]

        from common.cache.user import UserCollectionCache

        count,page_results = UserCollectionCache(g.user_id).get_page_collection(page,per_page)

        artilce_list=[]
        from common.cache.article import ArticleCache
        for article_id in page_results:

            article = ArticleCache(article_id).get_detail()

            artilce_list.append(article)

        # 6. 返回响应
        return {
            "page": page,
            "per_page": per_page,
            "total_count": count,
            "results": artilce_list
        }

class UnCollectionResouce(Resource):

    method_decorators = [login_required]

    def delete(self,target):

        try:
            ct = Collection.query.filter(
                Collection.user_id == g.user_id,
                Collection.article_id == target
            ).first()
        except Exception as e:
            current_app.logger.error(e)
            ct = None

        if ct :
            ct.is_deleted=True
            db.session.commit()


        return {'target':target}


"""
需求:
        登录用户 实现发布评论的功能
前端:
        当登录用户输入完评论内容之后,会发送ajax请求
        
        会携带 评论信息. 登录用户信息的token在请求头中
后端:
        
        1.接收数据
        2.验证数据 [我们可以对接 百度的 敏感数据验证]
        3.数据入库
        4.返回响应

"""
from common.models.news import Comment
class CommentResouce(Resource):

    method_decorators = {
        'post':[login_required]
    }

    def post(self):
        # 1.接收数据
        # 文章id
        # 评论文章为文章id，对评论进行回复则是评论id
        target = request.json.get('target')
        # 评论内容
        content = request.json.get('content')
        # 2.验证数据 [我们可以对接 百度的 敏感数据验证]

        #art_id(**body**)int否对文章评论不需要传递此参数。对评论内容进行回复时，必须传递此参数
        art_id = request.json.get('art_id')

        if art_id is None:

            # 3.数据入库
            cm = Comment(
                user_id=g.user_id,
                article_id=target,
                content=content
            )
            db.session.add(cm)
            db.session.commit()

        else:

            # 回复评论
            cm = Comment(
                user_id=g.user_id,
                article_id = art_id,
                parent_id = target,
                content=content
            )
            db.session.add(cm)

            #修改回复评论的数量
            parent_comment = Comment.query.get(target)
            parent_comment.reply_count += 1

            db.session.commit()



        # 4.返回响应
        return {
            "com_id": cm.id,
            "target": target
        }

    def get(self):

        """
        1. 根据 文章id 查询评论
        2. 查询的评论是对象列表,转换为字典列表
        3. 返回响应
        "total_count": 1,
        "end_id": 0,
        "last_id": 0,
        "results": [
            {
                "com_id": 108,
                "aut_id": 1155989075455377414,
                "aut_name": "18310820688",
                "aut_photo": "",
                "pubdate": "2019-08-07T08:53:01",
                "content": "你写的真好",
               "is_top": 0,
               "is_liking": 0
            }
        ]
        :return:
        """
        # 1. 根据 文章id 查询评论
        source = request.args.get('source')

        # type(**query**)str是评论类型，a表示文章评论 c表示回复评论
        type = request.args.get('type')
        if type == 'a':

            comments = Comment.query.filter(
                Comment.article_id == source,
                Comment.status == Comment.STATUS.APPROVED,
                Comment.parent_id == None
            ).all()
        else:

            # c表示回复评论
            comments = Comment.query.filter(
                Comment.parent_id == source,
                Comment.status == Comment.STATUS.APPROVED

            ).all()

        # 2. 查询的评论是对象列表,转换为字典列表
        results=[]



        from common.cache.user import CommentLikingCache
        commentlikings=CommentLikingCache(g.user_id).get_list()

        for comment in comments:



            results.append({
                'com_id': comment.id,
                'aut_id': comment.user.id,
                'aut_name': comment.user.name,
                'aut_photo': comment.user.profile_photo,
                'pubdate': comment.ctime.strftime('%Y-%m-%d %H:%M:%S'),
                'content': comment.content,
                'is_top': comment.is_top,
                'is_liking': comment.id in commentlikings,
                'reply_count': comment.reply_count
            })

        # 3. 返回响应
        return {
            "total_count": len(results),
            "end_id": 0,
            "last_id": 0,
            "results": results
        }

"""
需求:
        登录用户实现 对评论的 点赞和取消点赞
前端:
        当登录用户对某一个评论 点赞的时候 发送ajax请求
        携带 登录用户的token 和 评论id
后端:
        POST        JSON        target (comm_id)
        
        1. 接收数据
        2. 验证数据
        3. 查询数据 ,判断数据是否存在
        4. 如果数据不存在 则新增
        5. 如果数据存在,则更新
        6. 返回响应
        
"""
from common.models.news import CommentLiking
class CommentLikingsResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        # 1. 接收数据
        # 2. 验证数据
        target=request.json.get('target')
        # 3. 查询数据 ,判断数据是否存在
        cl = CommentLiking.query.filter(
            CommentLiking.user_id == g.user_id,
            CommentLiking.comment_id == target
        ).first()
        # 4. 如果数据不存在 则新增
        if cl is None:
            cl = CommentLiking(
                user_id=g.user_id,
                comment_id=target
            )

            db.session.add(cl)
            db.session.commit()
        else:
            # 5. 如果数据存在,则更新
            cl.is_deleted=False
            db.session.commit()

        from common.cache.user import CommentLikingCache
        CommentLikingCache(g.user_id).update(target)

        # 6. 返回响应
        return {'target':target}