# 蓝图的初始化 写在了 home的 __init__.py下
# 只要其他的模块 导入了 home 包. init肯定会执行

from flask.blueprints import Blueprint
from common.toutiaoapi.api import BaseApi

# 1. 创建蓝图实例对象
home_bp=Blueprint(name='home',import_name=__name__,url_prefix='/app/v1_0')
# 2. 让蓝图实例对象收集路由
# @home_bp.route('/home')
# def home():
#     return 'home'

# 创建Api实例对象
home_api=BaseApi(home_bp)

#路由
from . import views
home_api.add_resource(views.HomeResouce,'/home')


#获取所有频道
home_api.add_resource(views.AllChannelResouce,'/channels')

#获取登录用户关注的频道
home_api.add_resource(views.UserChannelResouce,'/user/channels')

#获取指定频道的文章列表
home_api.add_resource(views.ArticlesResouce,'/articles')

# 文章详情
home_api.add_resource(views.DetailResouce,'/articles/<article_id>')

#关注人物
home_api.add_resource(views.FollowingsResouce,'/user/followings')


home_api.add_resource(views.UnFollowingsResouce,'/user/followings/<target>')

# 点赞
home_api.add_resource(views.LikingResouce,'/article/likings')

# 取消点赞
home_api.add_resource(views.DisLikingResouce,'/article/likings/<target>')


# 不喜欢
home_api.add_resource(views.DislikesResouce,'/article/dislikes')

# 取消点赞
home_api.add_resource(views.UnDislikesResouce,'/article/dislikes/<target>')


#收藏文章
home_api.add_resource(views.CollectionResouce,'/article/collections')

#取消收藏
home_api.add_resource(views.UnCollectionResouce,'/article/collections/<target>')


# 发布评论
home_api.add_resource(views.CommentResouce,'/comments')

#点赞评论
home_api.add_resource(views.CommentLikingsResouce,'/comment/likings')
