
from flask import g

def login_required(func):

    def wrapper(*args,**kwargs):
        # 我们根据 token中的 用户id进行判断
        is_login = g.user_id

        # 如果传递的token是 刷新token 则禁止访问
        # 因为我们不允许使用 refresh_token来获取信息
        if g.is_refresh:
            return {'msg':'please login'},403

        # 登录用户
        if is_login :
            return func(*args,**kwargs)

        else:
            # 没有携带 用户信息
            return {'msg':'please login'},401

    return wrapper