
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

# from toutiao.settings import Config
# from toutiao import Config

from flask import current_app

from common.auth.auth_jwt import pyjwt_encode_data
from datetime import datetime,timedelta

def generic_login_token(user_id):

    # 1. 创建序列化器对象
    # s = Serializer(secret_key='abc',expires_in=24*3600)
    # s = Serializer(secret_key=Config.SECRET_KEY,expires_in=24*3600)
    # current_app.config.SECRET_KEY
    # current_app.config.get('SECRET_KEY')
    # s = Serializer(secret_key=current_app.config.get('SECRET_KEY'), expires_in=24 * 3600)
    # 2. 组织数据

    # 普通的token
    data = {
        'user_id':user_id,
        'is_refresh':False
    }
    short_exp_time = datetime.now() + timedelta(seconds=current_app.config.get('JWT_TOKEN_EXPIRE'))

    token = pyjwt_encode_data(data,expiry=short_exp_time)


    # 刷新的token
    data = {
        'user_id': user_id,
        'is_refresh': True
    }
    long_exp_time = datetime.now() + timedelta(seconds=current_app.config.get('JWT_REFRESHTOKEN_EXPIRE'))

    refresh_token = pyjwt_encode_data(data, expiry=long_exp_time)


    return token,refresh_token
    # 3. 返回token
    # return s.dumps(data).decode()

