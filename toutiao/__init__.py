from flask import Flask,request
from flask_sqlalchemy import SQLAlchemy
import redis
from flask_cors import CORS
from common.snowflake.id_worker import IdWorker

app = Flask(__name__)

from toutiao.settings import all_conf,setup_log
Config = all_conf.get('dev')

#加载日志
setup_log(Config)

#加载配置类
app.config.from_object(Config)

#创建SQLAlchem 实例对象
db = SQLAlchemy(app)

#创建redis连接对象
redis_cli = redis.Redis(host=Config.REDIS_HOST, port=Config.REDIS_PORT,db=Config.REDIS_DB)
app.redis_cli = redis_cli

#设置跨域 -- 课下可以看文档 来设置白名单
CORS(app)


#创建雪花算法 实例对象
worker = IdWorker(Config.DATACENTER_ID,         # 服务中心 位数
                  Config.WORKER_ID,             # 机器 位数
                  Config.SEQUENCE)              # 时序 位数
# 通过 app.属性=值
# 我们就可以通过 current_app.属性 来调用
app.worker=worker

# # 应该是在蓝图注册前边 注册转换器
from toutiao.utils.converters import MobileConverter
app.url_map.converters['phone']=MobileConverter

# 3.app要注册蓝图
from toutiao.apps.home import home_bp
app.register_blueprint(home_bp)

from toutiao.apps.user import user_bp
app.register_blueprint(user_bp)

@app.route('/')
def index():
    return 'index'


# 在每次请求前 我们都要获取 请求头中的 信息 看看有没有token
from common.auth.auth_jwt import pyjwt_check_token
from flask import g
@app.before_request
def before_request():

    g.user_id = None
    g.is_refresh=None
    # 1. 获取请求头中的 token数据
    authorization = request.headers.get('Authorization')
    # 2. 判断数据
    if authorization and authorization.startswith('Bearer '):
        # 3. 截取token
        token = authorization[7:]

        # 4. 对token进行解密
        data = pyjwt_check_token(token)
        if data is not None:
            g.user_id = data.get('user_id')
            g.is_refresh=data.get('is_refresh')

