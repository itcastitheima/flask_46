from toutiao import app,db
#为了一会抽取代码方便 我们把 脚本管理工具放在最下边
from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand

# 创建manager实例对象
manager=Manager(app)

# 让Migrate 关联 app和db
Migrate(app=app,db=db)
#我们需要使用脚本指令来 进行相关的迁移
# 所以要把迁移的指令 添加到Manager实例对象上
manager.add_command('db',MigrateCommand)

if __name__ == '__main__':
    manager.run()
